/*
 * bind.c
 *
 * Copyright (C) 2012 Joyabrata Ghosh
 * Contributed by Joyabrata Ghosh <jghosh@hawk.iit.edu>
 *
 * This file is part of lns-tools, a utility to add, delete and list
 * the current Logical Network Segmentation (lns) rules.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 */
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if.h>
#if 0
#include <linux/sockios.h>
#endif

int tag = 0;
struct in_addr ip;
struct in_addr destip;
char *intf = NULL;
unsigned short destport = 0;

int main(int argc, char *argv[]) 
{
	int c, s1, s2, s;
	struct sockaddr_in sin;
	int opt, ret;
	socklen_t optlen;
	char buf[50];
	struct ifreq interface;
	struct ifreq ifr;

	while ((c = getopt(argc, argv, "t:i:I:d:p:")) != -1) {
		switch(c) {
			case 't': 
				tag = atoi(optarg); 
				break;
			case 'i': 
				ip.s_addr = inet_addr(optarg);
				if (ip.s_addr == -1) {
					fprintf(stderr, "Invalid IP address: %s\n\n", optarg);
					exit(1);
				}
				break;
			case 'd': 
				destip.s_addr = inet_addr(optarg);
				if (destip.s_addr == -1) {
					fprintf(stderr, "Invalid Dest IP address: %s\n\n", optarg);
					exit(1);
				}
				break;
			case 'p': 
				destport = atoi(optarg);
				break;
			case 'I': 
				intf = strdup(optarg);
				memset(&ifr, 0, sizeof(struct ifreq));
				s = socket (PF_INET, SOCK_STREAM, 0);
				strncpy(ifr.ifr_name, intf, IFNAMSIZ);
				if (ioctl(s, SIOCGIFFLAGS, &ifr)) {
					fprintf(stderr, "Invalid Interface name: %s\n\n", optarg);
					exit(1);
				}
				break;

			default:
				printf("Invalid option\n");
				exit(1);
				break;
		}
	}

	s1 = socket(AF_INET, SOCK_STREAM, 0);
	printf("Socket with AF_INET and SOCK_STREAM\n");

	opt = 1;
	ret = setsockopt(s1, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int));
	if (ret < 0) {
		perror("setsockopt:SO_REUSEADDR");
		exit(1);
	}
	printf("Socket REUSEADDR set\n");

	if (tag) {
		printf("Associating with Tag=%d\n", tag);
		opt = tag;
/* this doesn't belong here!!! */
		#define SO_LNS_ATTACH_TAG 35
		ret = setsockopt(s1, SOL_SOCKET, SO_LNS_ATTACH_TAG, &opt, sizeof(int));
		if (ret < 0) {
			perror("setsockopt:SO_LNS_ATTACH_TAG");
			exit(1);
		}
	}

	if (intf) {
		printf("Bind on Device %s\n", intf);
  		strncpy(interface.ifr_ifrn.ifrn_name, intf, IFNAMSIZ);
  		if (setsockopt(s1, SOL_SOCKET, SO_BINDTODEVICE,
			      (char *)&interface, sizeof(interface)) < 0) {
     			perror("setsockopt:SO_BINDTODEVICE");
   	  		exit(1);
		}
	} 

	memset(&sin, 0, sizeof(struct sockaddr_in));
	sin.sin_family = AF_INET;
	if (destip.s_addr && destport)
		sin.sin_port   = 0;
	else
		sin.sin_port   = htons(destport);
	sin.sin_addr.s_addr = ip.s_addr;

	if (ip.s_addr || sin.sin_port) {
		if (ip.s_addr) 
			printf("Bind on %s on port %d\n", inet_ntoa(ip), destport);
		else
			printf("Bind on INADDR_ANY on port %d\n", destport);
		if (bind(s1, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
			perror("bind");
			exit(1);
		}
	}

	if (destip.s_addr && destport) {
		memset(&sin, 0, sizeof(struct sockaddr_in));
		sin.sin_family = AF_INET;
		sin.sin_port   = htons(destport);
		sin.sin_addr.s_addr = destip.s_addr;
	
		printf("Initiating Connect\n");
		if (connect(s1, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
			perror("connect");
			exit(1);
		}

		sprintf(buf, "hello world");
		send(s1, buf, strlen(buf), 0);
		printf("Sending data on Connected socket\n");
		sleep(3);
		exit(0);
	}

	printf("Listen initiated\n");
	if (listen(s1, 5) < 0) {
		perror("Listen");
		exit(1);
	}

	optlen = sizeof(struct sockaddr);
	while ((s2 = accept(s1, (struct sockaddr *)&sin, &optlen)) > 0) {
		printf("[%s] Accepted new sock, Connected, sending buf, closing new sock\n", argv[0]);
		sprintf(buf, "Your IP was %s\n", inet_ntoa(sin.sin_addr));
		send(s2, buf, strlen(buf), 0);
		sleep(3);
		close(s2);
	}
	exit(0);
}

