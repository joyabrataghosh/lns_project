.\"
.\" lns.conf
.\"
.\" Original: (jghosh@hawk.iit.edu)
.\"
.\"
.TH lns.CONF 5 "6 Oct 2012" "lns.conf" "Linux System Administration"
.SH NAME
lns.conf \- Logical Network Segmentation (lns) configuration file
.SH DESCRIPTION
The \fBlns.conf\f1 file is the main configuration file for lns.
It contains rules to be read in and set by \fBlnsconfig\f1(8).
The syntax is simply as follows:
.RS
.sp
.nf
.ne 7
# comment
rule

rule # comment
.fi
.sp
.RE
.PP
Note that blank lines are ignored, as are lines that begin with a #.
.SH EXAMPLE
.RS
.sp
.nf
.ne 7
# lns.conf sample
#
# add a rule for command foo, port 54321, interface eth0
-a -cmd foo -p 54321 -I eth0

-a -uid 1234 -I eth3 # restrict uid 1234 to eth3
.fi
.sp
.RE
.PP
.SH SEE ALSO
\fBlnsconfig\f1(8)
