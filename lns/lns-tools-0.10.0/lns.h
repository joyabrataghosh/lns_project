/*
 * lns.h
 *
 * Copyright (C) 2012 Joyabrata Ghosh
 * Contributed by Joyabrata Ghosh <jghosh@hawk.iit.edu>
 *
 * This file is part of lns-tools, a utility to add, delete and list
 * the current Logical Network Segmentation (lns) rules.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 */

#ifndef __LNS_H
#define __LNS_H

#include <bits/types.h>
#include <netinet/in.h>

#define		SO_LNS_TAG			35	/* for setsockopt() */
/* for backwards-compatibility */
#define		SO_LNS_ATTACH_TAG		SO_LNS_TAG

#define		LNS_MAX_CMD_NAME		64

#define		LNS_MAGIC			0x0BCF0BCF

/* user-visible API structure */
struct lns_arg {
	int		magic;		/* LNS_MAGIC */

	int		op;		/* lns_operations */
	int		rulenum;	/* rule number, if given */

	int		type;		/* lns_type */
	union {
		char		cmd[LNS_MAX_CMD_NAME];
		uid_t		uid;
		uint32_t	tag;
	} t;

	/* 16 bits for flags */
	short		deflt:1,	/* is a default rule */
			force:1,	/* force operation */
			ifbind:1,	/* bind address use to interface */
			first:1;	/* put entry first in bind_list */

	unsigned short	port;		/* port, or beginning port */
	unsigned short	port_end;

	unsigned short	family;		/* address family */
	union {
		uint32_t	v4;		/* AF_INET */
		struct in6_addr	v6;		/* AF_INET6 */
		unsigned int	ifindex;	/* AF_UNSPEC */
	} ai;
};

enum lns_operations {
	LNS_OP_LIST = 0,
	LNS_OP_ADD,
	LNS_OP_DEL,
	LNS_OP_RESET,
	LNS_OP_MAX,
};

enum lns_type {
	LNS_TYPE_CMD = 1,
	LNS_TYPE_UID,
	LNS_TYPE_TAG,
	LNS_TYPE_MAX,
};

#endif /* __LNS_H */
