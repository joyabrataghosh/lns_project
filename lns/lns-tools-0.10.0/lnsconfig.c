/*
 * lnsconfig.c
 *
 * Copyright (C) 2012 Joyabrata Ghosh
 * Contributed by Joyabrata Ghosh <jghosh@hawk.iit.edu>
 *
 * This file is part of lns-tools, a utility to add, delete and list
 * the current Logical Network Segmentation (lns) rules.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netdb.h>

#include "lns.h"

/* maybe this should be in lns.h */
#define LNS_PROC	"/proc/net/lns"

/* should be in netinet/in.h */
#ifndef IN6_EXTRACT_V4ADDR
#define IN6_EXTRACT_V4ADDR(a) \
	(((__const uint32_t *) (a))[3])
#endif

struct sockaddr_inx {
	union {
		struct sockaddr_in6 w6;
		struct sockaddr_in w4;
	} un;
};
#define sin_family un.w4.sin_family
#define sin_addr   un.w4.sin_addr
#define sin_port   un.w4.sin_port

#define sin6_family   un.w6.sin6_family
#define sin6_addr     un.w6.sin6_addr
#define sin6_port     un.w6.sin6_port

struct sockaddr_inx addr;
int addr_family = AF_UNSPEC;
char *hostname;

int op = LNS_OP_MAX;
long lrulenum;
int rulenum;
char *cmd = NULL;
unsigned long luid;
uid_t uid;
int uid_passed;
unsigned long ltag;
uint32_t tag;
int ip;
int help;
int deflt;
int force;
int ifbind;
int first;
int last;
unsigned int iport, iport_end;
unsigned short port;
unsigned short port_end;
unsigned int ifindex;
int verbose;

struct option longopts[] = {
   { "list",	no_argument, &op, LNS_OP_LIST },
   { "add",	no_argument, &op, LNS_OP_ADD },
   { "add",	no_argument, &op, LNS_OP_ADD },
   { "del",	no_argument, &op, LNS_OP_DEL },
   { "reset",	no_argument, &op, LNS_OP_RESET },
   { "help",	no_argument, &help, 1 },
   { "default",	no_argument, &deflt, 1 },
   { "force",	no_argument, &force, 1 },
   { "bind",	no_argument, &ifbind, 1 },
   { "first",	no_argument, &first, 1 },
   { "last",	no_argument, &last, 1 },
   { "verbose",	no_argument, &verbose, 1 },
   { "cmd",	required_argument, NULL, 'c' },
   { "uid",	required_argument, NULL, 'u' },
   { "tag",	required_argument, NULL, 't' },
   { "ip",	required_argument, NULL, 'i' },
   { "intf",	required_argument, NULL, 'I' },
   { "port",	required_argument, NULL, 'p' },
   { "porthigh",required_argument, NULL, 'P' },
   { "num",	required_argument, NULL, 'n' },
   { 0, 0, 0, 0 }
};

static void
print_help(void)
{
	printf("Usage: lnsconfig <op> [cmd|uid|tag|default] [ip|intf] ");
	printf("[port [porthigh]] [num] [-v]\n");
	printf("\n");
	printf("<op> must be ONE of: list, add, del, reset\n");
	printf("   --list, -l  : List the lns configurations\n");
	printf("   --add, -a   : Add the following lns config\n");
	printf("   --del, -d   : Delete the following lns config\n");
	printf("                 If only (cmd|uid|tag) present, all IPs|intf will be deleted\n");
	printf("   --reset, -r : Deletes all rules and disables lns\n");
	printf("\n");
	printf("followed by ONE of:\n");
	printf("   --cmd, -c     : Specifies the process command name\n");
	printf("   --uid, -u     : Specifies the uid\n");
	printf("   --tag, -t     : Specifies the tag id (value of 1 to 255)\n");
	printf("   --default, -D : Default case (if none of the rules match)\n");
	printf("\n");
	printf("followed by ONE of:\n");
	printf("   --ip, -i   : Specifies an existing IP(v6) address on the system\n");
	printf("   --intf, -I : Specifies an interface name\n");
	printf("\n");
	printf("Optional parameters:\n");
	printf("   --port, -p     : Specifies a port for rule filtering\n");
	printf("   --porthigh, -P : Specifies a high port (range) for rule filtering\n");
	printf("   --num, -n      : Specifies the rule number\n");
	printf("   --force, -f    : Force operation to complete\n");
	printf("   --bind, -b     : Bind address use to interface it is configured on\n");
	printf("   --first, -F    : Put entry first in the rule bind_list\n");
	printf("   --last, -L     : Put entry last in the rule bind_list\n");
	printf("   --verbose, -v  : Displays verbose information, if available\n");
	printf("   --help, -h     : Displays this message\n");
	printf("\n");
	exit(1);
}

/*
 * Resolve hostname 'name' into an IPv* address
 * address will be stored in 'to'
 * hostname will be stored in 'hbuf' (if passed)
 * 'family' is used to force a particular address family
 *
 * NOTES: 'to' MUST be zero'ed before calling
 *        'hbuf' MUST be MAXHOSTNAMELEN in size
 */
static void
resolve_host(char *name, struct sockaddr_inx *to, int family, char *hbuf)
{
	struct hostent *psHost;

	if (family != AF_INET6 && inet_aton(name, &to->sin_addr) == 1) {
		/* numeric V4 target */
		if (hbuf)
			strncpy(hbuf, name, MAXHOSTNAMELEN - 1);
		to->sin_family = AF_INET;
	} else {
		struct addrinfo hints, *ai;
		int ni;

		memset(&hints, 0, sizeof(struct addrinfo));

		/* numeric V6 target */
		if (family != AF_INET && inet_pton(AF_INET6, name, &to->sin6_addr) != 0) {
			if (hbuf)
				strncpy(hbuf, name, MAXHOSTNAMELEN - 1);
			to->sin_family = AF_INET6;
		} else {
			switch (family) {
			case AF_INET:
				psHost = gethostbyname(name);
				if (!psHost) {
					fprintf(stderr, "Error: Unknown host %s\n", name);
					exit(1);
				}
				to->sin_family = AF_INET;
				memcpy(&to->sin_addr, psHost->h_addr, 4);
				if (hbuf)
					strncpy(hbuf, psHost->h_name, MAXHOSTNAMELEN - 1);
				break;

			case AF_UNSPEC:
			case AF_INET6:
				hints.ai_family = family;
				hints.ai_flags = AI_ADDRCONFIG|AI_CANONNAME;
				if (family == AF_UNSPEC)
					hints.ai_flags |= AI_ALL;

				ni = getaddrinfo(name, NULL, &hints, &ai);

				if (ni != 0) {
					fprintf(stderr, "Error: Unknown host %s\n", name);
					exit(1);
				}

				to->sin_family = ai->ai_family;
				if (to->sin_family == AF_INET) {
					memcpy(&to->sin_addr, &((struct sockaddr_inx *)ai->ai_addr)->sin_addr, 4);
				} else {
					struct in6_addr *ina6;
					ina6 = &((struct sockaddr_inx *)ai->ai_addr)->sin6_addr;
					if (IN6_IS_ADDR_V4MAPPED(ina6)) {
						to->sin_family = AF_INET;
						to->sin_addr.s_addr = IN6_EXTRACT_V4ADDR(ina6);
					} else {
						memcpy(&to->sin6_addr, ina6, 16);
					}
				}

				if (hbuf)
					strncpy(hbuf, ai->ai_canonname, MAXHOSTNAMELEN - 1);
				if (ai)
					freeaddrinfo(ai);
				break;
			default:
				/* NOTREACHED */
				to->sin_family = AF_UNSPEC;
				return;
			}
		}
	}
	if (hbuf)
		hbuf[MAXHOSTNAMELEN - 1] = 0;
}

static void
print_host(struct sockaddr_inx *addr)
{
	char str[64];

	inet_ntop(addr->sin_family, &addr->sin6_addr, str, sizeof(str));
	printf("Resolved IP(v6) address: %s\n", hostname);
	if (addr->sin_family == AF_INET)
		printf("                numeric: %s\n", inet_ntoa(addr->sin_addr));
	else
		printf("                numeric: %s\n", str);
	printf("                 family: %d\n", addr->sin_family);
}

/* This function indicates whether or not s may represent a number.
 * It returns true iff s consists purely of digits and minuses.
 * Note that minus may appear more than once, and the empty
 * string will produce a `true' response.
 */
static int
isanum(char *s)
{
	while (*s == '-' || isdigit(*s))
		s++;
	return *s == '\0';
}

/*
 * This function indicates whether or not s may represent
 * a number.  It returns true iff s consists purely of digits.
 */
static void
portcheck(int p)
{
	if (p < 1 || p > 65535) {
		fprintf(stderr, "Error: Invalid port: %d\n\n", p);
		print_help();
	}
}

int main(int argc, char *argv[])
{
	int fd, ret, c;
	struct stat statbuf;
	char buf[1024], hnamebuf[MAXHOSTNAMELEN];
	struct lns_arg sarg;

	if (verbose)
		printf("LNSConfig v1.0 Allows program to bind to list of IPs|Interfaces only\n\n");

	while ((c = getopt_long(argc, argv, ":abc:DdFfhI:i:Lln:P:p:rt:u:v", longopts, NULL)) != -1) {
		switch (c) {
		case 'a': op = LNS_OP_ADD; break;
		case 'b': ifbind = 1; break;
		case 'c':
			cmd = optarg;
			break;
		case 'D': deflt = 1; break;
		case 'd': op = LNS_OP_DEL; break;
		case 'F': first = 1; break;
		case 'f': force = 1; break;
		case 'h': print_help(); break;
		case 'I':
			ifindex = if_nametoindex(optarg);
			if (ifindex == 0) {
				fprintf(stderr, "Error: Invalid interface name: %s\n\n", optarg);
				print_help();
			}
			break;
		case 'i':
			resolve_host(optarg, &addr, addr_family, hnamebuf);
			hostname = hnamebuf;
			addr_family = addr.sin_family;
			if (addr_family == AF_UNSPEC) {
				fprintf(stderr, "Error: Invalid IP(v6) address: %s\n\n", optarg);
				print_help();
			}
			ip = 1;
			break;
		case 'L': last = 1; break;
		case 'l': op = LNS_OP_LIST; break;
		case 'n':
			if (!isanum(optarg)) {
				fprintf(stderr, "Error: Invalid rule number, not a number: %s\n\n", optarg);
				print_help();
			}
			lrulenum = strtol(optarg, NULL, 10);
			if (lrulenum < 0 || lrulenum > INT_MAX) {
				fprintf(stderr, "Error: Invalid rule number: %ld\n\n", lrulenum);
				print_help();
			}
			rulenum = (int)lrulenum;
			break;
		case 'r': op = LNS_OP_RESET; break;
		case 'P':
			if (!isanum(optarg)) {
				fprintf(stderr, "Error: Invalid port, not a number: %s\n\n", optarg);
				print_help();
			}
			iport_end = atoi(optarg);
			portcheck(iport_end);
			port_end = (unsigned short)iport_end;
			break;
		case 'p':
			if (!isanum(optarg)) {
				fprintf(stderr, "Error: Invalid port, not a number: %s\n\n", optarg);
				print_help();
			}
			iport = atoi(optarg);
			portcheck(iport);
			port = (unsigned short)iport;
			break;
		case 't':
			if (!isanum(optarg)) {
				fprintf(stderr, "Error: Invalid port, not a number: %s\n\n", optarg);
				print_help();
			}
			ltag = strtoul(optarg, NULL, 10);
			if (ltag == 0 || ltag > UINT_MAX) {
				fprintf(stderr, "Error: Invalid tag: %lu\n\n", ltag);
				print_help();
			}
			tag = (uint32_t)ltag;
			break;
		case 'u':
			if (!isanum(optarg)) {
				fprintf(stderr, "Error: Invalid uid, not a number: %s\n\n", optarg);
				print_help();
			}
			luid = strtoul(optarg, NULL, 10);
			/* this should be the max uid */
			if (luid > (uid - 1)) {
				fprintf(stderr, "Error: Invalid uid: %lu\n\n", luid);
				print_help();
			}
			uid = (uid_t)luid;
			uid_passed = 1;
			break;
		case 'v': verbose = 1; break;
		case 0:		/* getopt_long() has found long option */
			break;
		case ':':	/* missing option argument */
			fprintf(stderr, "Error: Option '-%c' requires an argument\n\n",
					optopt);
			print_help();
			break;
		case '?':
		default:	/* invalid option */
			fprintf(stderr, "Error: Option '-%c' is invalid\n\n",
					optopt);
			print_help();
			break;
		}
	}

	if (help)
		print_help();

	if (verbose && ip)
		print_host(&addr);

	/* no operation given */
	if (op == LNS_OP_MAX) {
		fprintf(stderr, "Error: No operation specified\n\n");
		print_help();
	}

	/* If LNS_PROC is not present, then error */
	if (stat(LNS_PROC, &statbuf) != 0) {
		fprintf(stderr, "Error: File %s not present - unsupported kernel\n", LNS_PROC);
		exit(1);
	}

	if (op == LNS_OP_LIST) {
		/* Open the file and read */
		fd = open(LNS_PROC, O_RDONLY);
		if (fd < 0) {
			fprintf(stderr, "Error: Could not open file %s, %s",
					lns_PROC, strerror(errno));
			exit(1);
		}

		do {
			ret = read(fd, buf, sizeof(buf));
			if (ret > 0) {
				buf[ret] = 0;
				printf("%s", buf);
			}
		} while (ret > 0);
		close(fd);
		exit(0);
	}

	/* Check only one was given, there's got to be a better way... */
	if ((cmd && (uid_passed || tag || deflt)) ||
	    (uid_passed && (cmd || tag || deflt)) ||
	    (tag && (uid_passed || cmd || deflt)) ||
	    (deflt && (uid_passed || tag || cmd))) {
		fprintf(stderr, "Error: Only one of (cmd|uid|tag|default) can be specified\n\n");
		print_help();
	}

	/* Check only IP or Interface was given */
	if (ip && ifindex) {
		fprintf(stderr, "Error: Only one of (IP|intf) can be specified\n\n");
		print_help();
	}

	/* Add or delete, but all options not specified */
	if (op == LNS_OP_ADD && !((cmd || uid_passed || tag || deflt) && (ip || ifindex))) {
		fprintf(stderr, "Error: For add option, both (cmd|uid|tag|default) and (ip|intf) must be specified\n\n");
		print_help();
	}
	if (op == LNS_OP_DEL && !(cmd || uid_passed || tag || deflt || rulenum)) {
		fprintf(stderr, "Error: For delete option, one of (cmd|uid|tag|default|rulenum) must be specified\n\n");
		print_help();
	}

	if (deflt && rulenum) {
		rulenum = 0;
		if (verbose)
			fprintf(stderr, "Warning: Ignoring rulenum parameter with -D/--default\n");
	}

	if ((port && port_end && port > port_end) || (!port && port_end)) {
		fprintf(stderr, "Error: Invalid port range specified\n\n");
		print_help();
	}

	if (first && last) {
		fprintf(stderr, "Error: Only one of (first|last) can be specified\n\n");
		print_help();
	}

	/* Check if IP(v6) address exists */

	/* Open the file for writing */
	fd = open(LNS_PROC, O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "Error: Could not open file %s, %s", LNS_PROC,
				strerror(errno));
		exit(1);
	}

	memset(&sarg, 0, sizeof(sarg));
	sarg.magic = LNS_MAGIC;
	sarg.op = op;
	sarg.rulenum = rulenum;
	if (cmd) {
		sarg.type = LNS_TYPE_CMD;
		strncpy(sarg.t.cmd, cmd, LNS_MAX_CMD_NAME);
	} else if (uid_passed) {
		sarg.type = LNS_TYPE_UID;
		sarg.t.uid = uid;
	} else if (tag) {
		sarg.type = LNS_TYPE_TAG;
		sarg.t.tag = tag;
	}
	if (ip) {
		sarg.family = addr.sin_family;
		if (sarg.family == AF_INET)
			sarg.ai.v4 = addr.sin_addr.s_addr;
		else
			sarg.ai.v6 = addr.sin6_addr;
	} else if (ifindex) {
		sarg.family = 0;
		sarg.ai.ifindex = ifindex;
	}
		
	if (deflt)
		sarg.deflt = 1;
		
	if (force)
		sarg.force = 1;
		
	if (ifbind)
		sarg.ifbind = 1;
		
	if (first)
		sarg.first = 1;

	sarg.port = port;
	sarg.port_end = port_end;

	ret = write(fd, (char *)&sarg, sizeof(sarg));
	if (ret > 0) {
		if (verbose) {
			if (op == LNS_OP_ADD)
				printf("Rule added successfully\n");
			else if (op == LNS_OP_DEL)
				printf("Rule deleted successfully\n");
			else if (op == LNS_OP_RESET)
				printf("All rules reset/deleted\n");
		}
		ret = 0;
	} else {
		switch (errno) {
			case ENOMEM:
				fprintf(stderr, "Error: Add failed, kernel rule allocation failure\n");
				break;
			case EEXIST:
				fprintf(stderr, "Error: Add failed, config already present, or duplicate rule number\n");
				break;
			case EDESTADDRREQ:
				fprintf(stderr, "Error: Add failed, IP(v6) address not given\n");
				break;
			case ENODEV:
				fprintf(stderr, "Error: Add failed, interface does not exist\n");
				break;
			case EPROTOTYPE:
				fprintf(stderr, "Error: Add failed, address and interface rules cannot be combined\n");
				break;
			case EAFNOSUPPORT:
				fprintf(stderr, "Error: Add failed, address family not supported\n");
				break;
			case EADDRNOTAVAIL:
				fprintf(stderr, "Error: Add failed, address not available\n");
				break;
			case ENOENT:
				fprintf(stderr, "Error: Delete failed, config not present\n");
				break;
			case EBUSY:
				fprintf(stderr, "Error: Operation failed, rule in use\n");
				break;
			case EOPNOTSUPP:
				fprintf(stderr, "Error: Operation failed, operation not supported\n");
				break;
			case EINVAL:
				fprintf(stderr, "Error: Operation failed, invalid argument\n");
				break;
			default:
				fprintf(stderr, "Error: Operation failed, internal error - %s\n", strerror(errno));
				break;
		}
		ret = 1;
	}
	close(fd);
	exit(ret);
}

