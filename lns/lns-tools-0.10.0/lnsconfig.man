.\"
.\" lnsconfig
.\"
.\" Original: (jghosh@hawk.iit.edu)
.\"
.\"
.TH LNSCONFIG 8 "29 Oct 2012" "lnsconfig" "Linux Netwrk Administration"
.SH NAME
lnsconfig \- Add/Delete/List Logical Network Segmentation (lns) rules
.SH SYNOPSIS
\fBlnsconfig\f1 <op> [cmd|uid|tag|default] [ip|intf]
[port [porthigh]] [num] [-f] [-v]
.SH DESCRIPTION
The \fBlnsconfig\f1 utility can add, delete and list the current lns
configuration for the system.
.SH COMMAND SYNTAX
.TP "15"
The following operations are valid:
.TP
\fB-l, --list\f1
Lists the rules installed in the kernel database.
.TP
\fB-a, --add\f1
Adds a rule to the kernel database.
.TP
\fB-d, --del\f1
Deletes a rule from the kernel database.
.TP
\fB-r, --reset\f1
Deletes all rules and resets the kernel database.
.P
The following qualifiers are valid with \fB-a\f1 and \fB-d\f1:
.TP
\fB-c, --cmd\f1 \f2command\f1
Specifies a \f2command name\f1 for the rule being added or deleted.
.TP
\fB-u, --uid\f1 \f2uid\f1
Specifies the \f2UID\f1 for the rule.
.TP
\fB-t, --tag\f1 \f2tag\f1
Specifies the \f2tag\f1 for the rule.  A \f2tag\f1
is an integer used to match this rule with a corresponding
setsockopt(SO_LNS_TAG) call used on a socket.
.TP
\fB-D, --default\f1
Specifies this is a default rule.
.TP
\fB-i, --ip\f1
Specifies an existing IP(v6) address on the system.
.TP
\fB-I, --intf\f1
Specifies an interface name.
.SH OPTIONS
.TP "15"
The following options are valid:
.TP
\fB-p\f1 \f2port\f1
Specifies a port for rule filtering.  Optionally specifies the lower
bound of a port range when used with \fB-P\f1 \f2porthigh\f1.
.TP
\fB-P\f1 \f2porthigh\f1
Specifies the upper bound of a port range for rule filtering.  Only valid
with \fB-p\f1 \f2port\f1.
.TP
\fB-n\f1 \f2rulenum\f1
Specifies the rule number to use in the kernel database.  If not given,
it will be created with the next highest rule number available.
.TP
\fB-f, --force\f1
Force operation to complete, even if the kernel would normally not
allow it.  This can lead to inconsistencies in rule enforcement so
should only be used when absolutely necessary.
.TP
\fB-b, --bind\f1
Specifies that an address rule should be bound to the interface it is
configured on, instead of being able to use any interface on the
system.
.TP
\fB-F, --first\f1
Specifies that the interface or address specified should be inserted
as the first entry in the rule.
.TP
\fB-L, --last\f1
Specifies that the interface or address specified should be inserted
as the last entry in the rule.
.TP
\fB-v\f1
Display verbose information, if available.
.SH EXIT CODES
\fBlnsconfig\f1 returns one of the following exit codes:
.TP 
\fB0\f1
Successful completion.
.TP 
\fB1\f1
An error occurred.
.SH EXAMPLES
.LP
To list all rules installed in the kernel database:
.RS
.nf
\fBlnsconfig -l\fP
.fi
.RE
.LP
To add a rule to restrict telnet to the loopback address, assuming
it is being started from xinetd:
.RS
.nf
\fBlnsconfig -a -c xinetd -i 127.0.0.1 -p 23\fP
.fi
.RE
.SH SEE ALSO
\fBnetstat\f1(8), \fBip\f1(8), \fBsetsockopt\f1(2)
